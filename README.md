# runner-k8s-deployment-sim

Simulation of jobs that take a long time to stop (e.g. gitlab-runner).

This is in support of [deploying gitlab-runner to k8s](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/4813).

## TLDR

To see the behaviour of deployments during long-running shutdown:

```
kubectl apply -f deployment.yaml
kubectl set env deployment/runner VERSION=v2
kubectl set env deployment/runner VERSION=v3
kubectl set env deployment/runner VERSION=v4
```

## Usage

### Creation

Assuming you have a kubernetes cluster running (e.g. `kind` or `minikube`), you can create the deployment:

```
kubectl apply -f deployment.yaml
```

Now you can see the pods being created:

```
kubectl get pods

NAME                      READY   STATUS              RESTARTS   AGE
runner-84f8f99cd5-42vsj   0/1     ContainerCreating   0          4s
runner-84f8f99cd5-5gwxz   0/1     ContainerCreating   0          4s
runner-84f8f99cd5-bvxqt   0/1     ContainerCreating   0          4s
runner-84f8f99cd5-xgt42   0/1     ContainerCreating   0          4s
runner-84f8f99cd5-z557l   0/1     ContainerCreating   0          4s
```

We can look at logs:

```
stern runner

+ runner-84f8f99cd5-xgt42 › runner
+ runner-84f8f99cd5-z557l › runner
+ runner-84f8f99cd5-bvxqt › runner
+ runner-84f8f99cd5-5gwxz › runner
+ runner-84f8f99cd5-42vsj › runner
runner-84f8f99cd5-xgt42 runner booting
runner-84f8f99cd5-xgt42 runner working on job 0
runner-84f8f99cd5-42vsj runner booting
runner-84f8f99cd5-bvxqt runner booting
runner-84f8f99cd5-bvxqt runner working on job 0
runner-84f8f99cd5-42vsj runner working on job 0
runner-84f8f99cd5-5gwxz runner booting
runner-84f8f99cd5-5gwxz runner working on job 0
runner-84f8f99cd5-z557l runner booting
runner-84f8f99cd5-z557l runner working on job 0
```

### Deploying a new version

Now, let's deploy a new set of runners:

```
kubectl set env deployment/runner VERSION=v2
```

We see new pods running, old ones shutting down:

```
kubectl get pods
NAME                      READY   STATUS        RESTARTS   AGE
runner-78667658c4-9dc4k   1/1     Running       0          24s
runner-78667658c4-jpts2   1/1     Running       0          24s
runner-78667658c4-krk7d   1/1     Running       0          24s
runner-78667658c4-tf2ss   1/1     Running       0          24s
runner-78667658c4-tp5nr   1/1     Running       0          24s
runner-84f8f99cd5-42vsj   1/1     Terminating   0          3m47s
runner-84f8f99cd5-5gwxz   1/1     Terminating   0          3m47s
runner-84f8f99cd5-bvxqt   1/1     Terminating   0          3m47s
runner-84f8f99cd5-xgt42   1/1     Terminating   0          3m47s
runner-84f8f99cd5-z557l   1/1     Terminating   0          3m47s
```

The logs show the same thing:

```
+ runner-78667658c4-jpts2 › runner
runner-78667658c4-jpts2 runner booting
runner-78667658c4-jpts2 runner working on job 0
runner-84f8f99cd5-5gwxz runner received signal: interrupt
runner-84f8f99cd5-5gwxz runner shutting down gracefully...
+ runner-78667658c4-tp5nr › runner
runner-78667658c4-tp5nr runner booting
runner-78667658c4-tp5nr runner working on job 0
runner-84f8f99cd5-42vsj runner received signal: interrupt
runner-84f8f99cd5-42vsj runner shutting down gracefully...
+ runner-78667658c4-krk7d › runner
runner-78667658c4-krk7d runner booting
runner-78667658c4-krk7d runner working on job 0
runner-84f8f99cd5-z557l runner received signal: interrupt
runner-84f8f99cd5-z557l runner shutting down gracefully...
+ runner-78667658c4-tf2ss › runner
runner-78667658c4-tf2ss runner booting
runner-78667658c4-tf2ss runner working on job 0
runner-84f8f99cd5-bvxqt runner received signal: interrupt
runner-84f8f99cd5-bvxqt runner shutting down gracefully...
+ runner-78667658c4-9dc4k › runner
runner-78667658c4-9dc4k runner booting
runner-78667658c4-9dc4k runner working on job 0
runner-84f8f99cd5-xgt42 runner received signal: interrupt
runner-84f8f99cd5-xgt42 runner shutting down gracefully...
```

We can now wait for the old ones to shut down. After a few minutes:

```
runner-84f8f99cd5-bvxqt runner done
runner-84f8f99cd5-xgt42 runner done
- runner-84f8f99cd5-xgt42 › runner
- runner-84f8f99cd5-bvxqt › runner
runner-84f8f99cd5-z557l runner done
- runner-84f8f99cd5-z557l › runner
runner-84f8f99cd5-42vsj runner done
- runner-84f8f99cd5-42vsj › runner
runner-84f8f99cd5-5gwxz runner done
- runner-84f8f99cd5-5gwxz › runner
```

The old pods are gone, we now have only the new ones:

```
kubectl get pods

NAME                      READY   STATUS    RESTARTS   AGE
runner-78667658c4-9dc4k   1/1     Running   0          4m24s
runner-78667658c4-jpts2   1/1     Running   0          4m24s
runner-78667658c4-krk7d   1/1     Running   0          4m24s
runner-78667658c4-tf2ss   1/1     Running   0          4m24s
runner-78667658c4-tp5nr   1/1     Running   0          4m24s
```

### Deploying multiple versions concurrently

Next, let's see what happens if we deploy a new version while the old is still shutting down.

```
kubectl set env deployment/runner VERSION=v3

...

kubectl set env deployment/runner VERSION=v4
```

We now have two sets of pods shutting down, but it didn't block the new ones coming up:

```
kubectl get pods
NAME                      READY   STATUS        RESTARTS   AGE
runner-6bc4fc4b4c-2kh28   1/1     Terminating   0          22s
runner-6bc4fc4b4c-2mspc   1/1     Terminating   0          22s
runner-6bc4fc4b4c-mp7tk   1/1     Terminating   0          22s
runner-6bc4fc4b4c-qt66q   1/1     Terminating   0          22s
runner-6bc4fc4b4c-th4zz   1/1     Terminating   0          22s
runner-78667658c4-9dc4k   1/1     Terminating   0          5m26s
runner-78667658c4-jpts2   1/1     Terminating   0          5m26s
runner-78667658c4-krk7d   1/1     Terminating   0          5m26s
runner-78667658c4-tf2ss   1/1     Terminating   0          5m26s
runner-78667658c4-tp5nr   1/1     Terminating   0          5m26s
runner-7b4ccf7bd4-5hkvr   1/1     Running       0          6s
runner-7b4ccf7bd4-hkqrg   1/1     Running       0          6s
runner-7b4ccf7bd4-phj25   1/1     Running       0          6s
runner-7b4ccf7bd4-q8jws   1/1     Running       0          6s
runner-7b4ccf7bd4-zglfl   1/1     Running       0          6s
```

This effectively means we can keep deploying new versions (or roll back) in the presence of long-running shutdowns.

## Implementation notes

1. The first is that in `Dockerfile` we need to override the `STOPSIGNAL`. It defaults to `SIGTERM`, but for a graceful shutdown, we'll want `SIGINT`:

    ```
    STOPSIGNAL SIGINT
    ```

2. We can create new pods ahead of time by setting `maxUnavailable` to `0`. By setting `maxSurge` to `100%` we can stop receiving jobs on _all_ current runners at the same time. We don't lose any capacity in the process, so this is equivalent to blue-green, but works on-demand.

    ```
    apiVersion: apps/v1
    kind: Deployment
    spec:
      strategy:
        rollingUpdate:
          maxUnavailable: 0
          maxSurge: '100%'
    ```

3. We need to ensure kubernetes waits for the pod to shutdown. The default grace period is 30 seconds, but this can be raised via the `terminationGracePeriodSeconds` parameter. That parameter is respected on all terminations, and I believe also when draining nodes.
