package main

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

var once sync.Once

func main() {
	sigs := make(chan os.Signal, 1)
	done := make(chan interface{})

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	duration := 10 * time.Second
	if os.Getenv("JOB_DURATION") != "" {
		var err error
		duration, err = time.ParseDuration(os.Getenv("JOB_DURATION"))
		if err != nil {
			panic(err)
		}
	}

	fmt.Println("booting")

	go func() {
		for {
			sig := <-sigs
			fmt.Printf("received signal: %v\n", sig)
			if sig == syscall.SIGTERM {
				fmt.Println("shutting down right away...")
				os.Exit(0)
			}
			once.Do(func() {
				fmt.Println("shutting down gracefully...")
				close(done)
			})
		}
	}()

	i := 0
loop:
	for {
		select {
		case <-done:
			break loop
		default:
			fmt.Printf("working on job %v\n", i)
			time.Sleep(duration)
			i++
		}
	}

	fmt.Println("done")

}
